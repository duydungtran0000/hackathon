import React, { Component } from "react";
import Logo from "../asset/image/logo-adw.png";
import { Link, animateScroll as scroll } from "react-scroll";
import './navbar-component.css';

class NavbarComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isOpen: false,
            userName: '',
            password: ''
        }
        this.login = this.login.bind(this);
    }

    openModal() {
        document.getElementById('id-modal').style.display = "block";
    }

    closeModal() {
        document.getElementById('id-modal').style.display = "none";
    }

    scrollToTop = () => {
        scroll.scrollToTop();
    };

    handleUsername(text) 
    {
        this.setState({userName:text.target.value});
    }

    handlePassword(text)
    {
        this.setState({password:text.target.value});
    }

    login(e) {
        e.preventDefault();
        console.warn("all state")
        let obj = {}
        obj.email = this.state.userName;
        obj.password = this.state.password;

        fetch('http://localhost:8080/login',
            {
                headers: {
                    "Content-Type": "application/json"
                },
                method: 'POST',
                body: JSON.stringify({obj})
            }
        ).then(response => console.log(response.json()))
    }

    render() {
        return (
            <nav className="nav" id="navbar">
                <div className="nav-content">
                    <img
                        src={Logo}
                        className="nav-logo"
                        alt="Logo"
                        onClick={this.scrollToTop}
                    />
                    <div className="nav_links">
                        <button className="underline-animation">
                            <Link
                                activeClass="active"
                                to="section2"
                                spy={true}
                                smooth={true}
                                offset={-70}
                                duration={300}
                            >
                                nos valeurs
              </Link>
                        </button>
                        <button className="underline-animation">
                            <Link
                                activeClass="active"
                                to="section3"
                                spy={true}
                                smooth={true}
                                offset={-70}
                                duration={300}
                            >
                                tests
              </Link>
                        </button>
                        <button className="underline-animation">
                            <Link
                                activeClass="active"
                                to="section4"
                                spy={true}
                                smooth={true}
                                offset={-70}
                                duration={300}
                            >
                                formations
              </Link>
                        </button>
                    </div>
                    <div className="button-item">
                        <button onClick={this.openModal}>connection</button>
                    </div>
                </div>

                <div id="id-modal" className="modal">
                    <form className="modal-content animate" onSubmit={this.login}>
                        <div className="imgcontainer">
                            <span onClick={this.closeModal} className="close" title="Close Model">&times;</span>
                        </div>

                        <div className="container">
                            <label htmlFor="uname"><b>username</b></label>

                            <input type="text" placeholder="Enter Username" onChange={(text) => { this.handleUsername(text)} } required />
                            <label htmlFor="psw"><b>password</b></label>

                            <input type="password" placeholder="Enter Password" onChange={(text) => { this.handlePassword(text)} } required />
                            <div className="button-item">
                                <button type="submit">Login</button>
                            </div>
                        </div>
                    </form>
                </div>
            </nav>
        )
    }
}

export default NavbarComponent;