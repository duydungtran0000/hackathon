import React, { Component } from "react";
import "./formulaire.css";
import Picto from "../asset/icon/picto.png";
import Estime from "../asset/image/estime.png";

class Formulaire extends Component {
    constructor(props) {
        super(props);
        this.formulare = null;
    }

    render() {
        if (this.props.id === "graphisme") {
            this.formulare = (
                <form className="modal-formulaire-content animate">
                    <div className="imgcontainer">
                        <span onClick={this.props.closeGraphisme} className="close-formulaire" title="Close Model">&times;</span>
                    </div>
                    <div class="title-box">
                        <p>Test</p>
                        <p>Graphisme</p>
                    </div>
                    <div class="box">
                        <div >
                            <h4> 1. Quel typographie, grasse et taille utiliserais
                                <br />tu pour un titre d'affiche
                            </h4>
                            <input type="checkbox" />
                            <label> Helvetica, 11 pt, regular</label>
                            <input type="checkbox" />
                            <label> Gala w01 condesed, 60pt, regular</label>
                            <input type="checkbox" />
                            <label> Avenir next, 10pt, utra light</label>
                            <br></br>
                            <h4>2. Dans le cadre de votre travail ou
                                <br />puisez vous votre energie
                            </h4>
                            <input type="checkbox" />
                            <label> En me concentrant sur seul projet</label>
                            <input type="checkbox" />
                            <label> Je prefere travailler à plusieur</label>
                            <input type="checkbox" />
                            <label> Je prefere etre seul</label>
                            <br></br>
                            <h4>3. Jauge ton niveau sur IN DESIGN?</h4>
                            <img
                                src={Estime}
                                className="estime-logo"
                                alt="Jauge ton niveau"
                            />
                            <h4>4. Quel serait le meilleur logiciel
                                <br />pour faire du MOTION DESIGN
                            </h4>
                            <input type="checkbox" />
                            <label> INDESIGN</label>
                            <input type="checkbox" />
                            <label> PREMIERE</label>
                            <input type="checkbox" />
                            <label> AFTER EFFECT</label>
                            <h4>5. Comment recupéreriez-vous une
                                <br />couleur sur un fichier existant?
                            </h4>
                            <input type="checkbox" />
                            <label> ABCD </label>
                            <input type="checkbox" />
                            <label> PIPETTE</label>
                            <input type="checkbox" />
                            <label> REPONSE</label>
                        </div>
                        <div>
                            <h4>6. Jauge ton niveau sur photoshop</h4>
                            <img
                                src={Estime}
                                className="estime-logo"
                                alt="Jauge ton niveau"
                            />
                            <h4>7. Quel outil utiliserais-tu sur PS
                                <br />pour appliquer un motif sur une forme
                            </h4>
                            <input type="checkbox" />
                            <label> LA PIPETTE</label>
                            <input type="checkbox" />
                            <label> MASQUE DE FUSION</label>
                            <input type="checkbox" />
                            <label> RECADRAGE</label>
                            <h4>8. Jauge ton niveau sur ILLUSTRATOR?</h4>
                            <img
                                src={Estime}
                                alt="Jauge ton niveau"
                            />
                            <h4>9. Jauge ton niveau sur AFTER EFFECTS?</h4>
                            <img
                                src={Estime}
                                alt="Jauge ton niveau"
                            />
                            <br></br>
                            <h4>10. Jauge ton niveau sur PREMIERE?</h4>
                            <img
                                src={Estime}
                                alt="Jauge ton niveau"
                            />
                            <h4>Portfolio</h4>
                            <img
                                src={Picto}
                                className="formu-logo"
                                alt="Portfolio"
                            /> <br></br>
                            <h4>CV</h4>
                            <img
                                src={Picto}
                                className="formu-logo"
                                alt="CV"
                            /> <br></br>
                        </div>
                    </div>
                    <textarea className="commentaire" name="message" rows="10" cols="115">Autre commentaire</textarea>
                    <div className="valide">
                        <button type="submit">Valider</button>
                    </div>
                </form>)
        } else if (this.props.id === "dev") {
            this.formulare = (
                <form className="modal-formulaire-content animate">
                    <div className="imgcontainer">
                        <span onClick={this.props.closeDev} className="close-formulaire" title="Close Model">&times;</span>
                        <div class="title-box">
                            <p>Test</p>
                            <p>Dev</p>
                        </div>
                    </div>
                </form>)
        } else if (this.props.id === "chef") {
            this.formulare = (
                <form className="modal-formulaire-content animate">
                    <div className="imgcontainer">
                        <span onClick={this.props.closeChef} className="close-formulaire" title="Close Model">&times;</span>
                        <div class="title-box">
                            <p>Test</p>
                            <p>Chef de projet</p>
                        </div>
                    </div>
                </form>)
        }
        return (
            <div>
                {this.formulare}
            </div>
        )
    }
}

export default Formulaire