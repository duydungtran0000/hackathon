import React, { Component } from "react";
import NavbarComponent from "../NavBarComponent/navbar-component"
import Home from "../Home/home";
import Test from "../Test/test";
import About from "../About/about";
import Formation from "../Formation/formation";
import Footer from "../Footer/footer";

class HeaderComponent extends Component {
    state = {};
    render() {
        return (
            <div>
                <NavbarComponent />
                    <Home/>
                    <About/>
                    <Test/>
                    <Formation/>
                    <Footer/>
            </div>
        )
    }
}

export default HeaderComponent