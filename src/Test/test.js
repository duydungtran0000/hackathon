import React, { Component } from "react";
import "./test.css"
import Formulaire from "../Formulaire/formulaire";

class Tests extends Component {
  constructor(props) {
    super(props);
    this.state = {
      idGraphisme: "graphisme",
      idDev: "dev",
      idChef: "chef"
    }
  }

  /**Not optimal */
  openGraphisme() {
    document.getElementById('id-graphisme').style.display = "block";
  }

  closeGraphisme() {
    document.getElementById('id-graphisme').style.display = "none";
  }

  openDev() {
    document.getElementById('id-dev').style.display = "block";
  }

  closeDev() {
    document.getElementById('id-dev').style.display = "none";
  }

  openChef() {
    document.getElementById('id-chef').style.display = "block";
  }

  closeChef() {
    document.getElementById('id-chef').style.display = "none";
  }
  render() {
    return (
      <div className="test-content" id="section3">
        <h1 className="test-title">Tests</h1>
        <div className="wrapper">
          <div className="wrapper-box-1">
            <button onClick={this.openGraphisme}>
              graphisme
            </button>
          </div>
          <div className="wrapper-box-2">
            <button onClick={this.openDev}>
              dev
          </button>
          </div>
          <div className="wrapper-box-3">
            <button onClick={this.openChef}>
              chef de projet
          </button>
          </div>
        </div>
        <div id="id-graphisme" className="modal-formulaire">
          <Formulaire
            id={this.state.idGraphisme}
            closeGraphisme={this.closeGraphisme}
          />
        </div>
        <div id="id-dev" className="modal-formulaire">
          <Formulaire
            id={this.state.idDev}
            closeDev={this.closeDev}
          />
        </div>
        <div id="id-chef" className="modal-formulaire">
          <Formulaire
            id={this.state.idChef}
            closeChef={this.closeChef}
          />
        </div>
      </div>
    );
  }
}

export default Tests;