import React, { Component } from "react";
import "./about.css"
class About extends Component {
  state = {};
  render() {
    return (
      <div className="about-content" id="section2">
        <h1 >
          Nos Valeurs
        </h1>
        <div className="about-body">
          <h3>Conseil, Ethique, Accompagnement</h3>
          <p>
              Nous sommes 148, une agence créative & technique, amoureux des plantes et du digital. Developpeurs Back-End, 
              Front-End ou full stack, chefs de projet, community manager, directeurs artistiques, concepteur rédacteur, 
              social media manager, motion designer...Découvrez la diversité des profils de l’agence. 
              C’est grâce à cette polyvalence de compétences que nous accompagnons nos clients, du Conseil en passant par la 
              Création jusqu’à la mise en œuvre. Nous n’avons pas que des idées, nous les réalisons !
          </p>

          <a className="about-link" href="https://148.fr/agence-eco-responsable" target="_blank">
            www.148.fr
            </a>
        </div>
      </div>
    );
  }
}

export default About;