import React, { Component } from "react";
import Home from "../Home/home";
import Test from "../Test/test";
import About from "../About/about";
import Formation from "../Formation/formation";

class Body extends Component {
    render() {
        return (
            <div>
                <Home
                    title="Home"
                    id="section1"
                />
                <About
                    title="Nos Valeurs"
                    id="section2"
                />
                <Test
                    title="Tests"
                    id="section3"
                />
                <Formation
                    title="Formations"
                    id="section4"
                />
            </div>
        )
    }
}

export default Body