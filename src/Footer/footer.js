import React, { Component } from "react";
import "./footer.css";
import Facebook from "../asset/icon/facebook.png";
import Instagram from "../asset/icon/instagram.png";
import Twitter from "../asset/icon/twitter.png";
import Linkedin from "../asset/icon/linkedin.png";
import Logo from "../asset/image/logo-white.png";
class Footer extends Component {
    state = {};
    render() {
        return (
            <div className="footer-content">
                <div>
                    <img
                        src={Logo}
                        className="logo"
                    />
                </div>
                <div>
                    <label>2 rue du Dohomey, 75011 Paris</label>
                    <label> . </label>
                    <label>contact@148.fr</label>
                    <label> . </label>
                    <label>+33(1) 42 72 92 37</label>
                </div>
                <div>
                    <img
                        src={Facebook}
                        className="social-logo"
                        alt="Facebook"
                    />
                    <img
                        src={Instagram}
                        className="social-logo"
                        alt="Instagram"
                    />
                    <img
                        src={Twitter}
                        className="social-logo"
                        alt="Twitter"
                    />
                    <img
                        src={Linkedin}
                        className="social-logo"
                        alt="Linkedin"
                    />
                </div>
            </div>
        );
    }
}

export default Footer;